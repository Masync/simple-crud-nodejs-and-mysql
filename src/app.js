const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mysql = require('mysql');
const expressMyconnection = require('express-myconnection');
const _cRoutes = require('./routes/customer');


/** init */
const app = express();

/** Settings */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/** Middlewares */
app.use(express.urlencoded({extended: false}));
app.use(morgan('combined'));
app.use(expressMyconnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Juan170922$',
    port: '3306',
    database: 'crudnodejs',
    insecureAuth : true
}, 'single'));

/** Routes */
app.use('/',_cRoutes);

/** Handler 404 */
app.use((req, res)=>{
    res.status(404).render('404page');

});
//static files
app.use(express.static(path.join(__dirname,'public')));

/**Start */
app.listen(app.get('port'), () =>{
    console.log('Server on port',  app.get('port'));
});