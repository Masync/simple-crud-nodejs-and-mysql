const controller = {};

controller.list =  (req, res) =>{
    req.getConnection((err, conn)=>{
        conn.query('select id,name,address,phone from customer', (err, customers)=>{
            if (err) {
                res.json(err);
            }
            console.log(customers);
            
            res.render('customers',{
                data: customers
            });
        })
    })
};
controller.form = (req, res) =>{
    res.render('form');
}
controller.save = (req, res) =>{
    const data = req.body;
    req.getConnection((err, conn)=>{
        conn.query('INSERT INTO customer set ?', [data], (err, customers)=>{
            if (err) {
                res.json(err);
            }
            
            res.redirect('/');
        });
    });
}
controller.edit = (req, res) =>{
    const {id} = req.params;
    req.getConnection((err, conn)=> {
        conn.query('SELECT id,name,address,phone FROM customer WHERE id = ?', [id], (err, row) => {
            res.render('edit', {
                data: row[0]
            });
        });
    });

}
controller.update = (req, res) =>{
    const {id} = req.params;
    const newData = req.body;

    req.getConnection((err, conn)=> {
        conn.query('UPDATE customer set ?  WHERE id = ?', [newData,id], (err, row) => {
            res.redirect('/');
        });
    });
}
controller.delete = (req, res) =>{
   const {id} = req.params;

   req.getConnection((err, conn)=> {
       conn.query('DELETE FROM customer WHERE id = ?', [id], (err, row) => {
           res.redirect('/');
       });
   });
}


module.exports = controller;