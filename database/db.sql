
CREATE DATABASE crudnodejs;

-- *using database
use crudnodejs;


-- *Create table
CREATE TABLE customer
(
     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(50) NOT NULL,
    address VARCHAR(100) NOT NULL,
     phone VARCHAR(15) NOT NULL
);

-- show all tables
SHOW TABLES;

-- DESCRIBE TABLE
DESCRIBE customer;
-- select table
select id,name,address,phone from customer;